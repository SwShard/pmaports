# Maintainer: Ultracoolguy <ultracoolguy@disroot.org>
# Kernel config based on: defconfig, msm8953.config, and device configs

_flavor="postmarketos-qcom-msm8953"
pkgname=linux-$_flavor
pkgver=6.0.10
pkgrel=0
pkgdesc="Close to mainline linux kernel for Qualcomm Snapdragon MSM8953"
arch="aarch64"
url="https://github.com/msm8953-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-iwd
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-waydroid
	"
makedepends="
	bash
	bison
	findutils
	flex
	openssl-dev
	perl
	postmarketos-installkernel
	"

# Source
_commit="e5486161bb0ce7d889d0a329a062b12864e61a56"

source="
	$pkgname-$_commit.tar.gz::https://github.com/msm8953-mainline/linux/archive/$_commit.tar.gz
	config-$_flavor.aarch64
"

_carch="arm64"

builddir="$srcdir/linux-$_commit"

prepare() {
	default_prepare
	cp -v "$srcdir/config-$_flavor.$CARCH" "$builddir"/.config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -Dm644 "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
2f661de119def3d0a28535a5b9107abdd41a04625f7c9fe5a125c24d502874ec4ba732ffd621c7dcf0eb39768711a9d0c24a992d2d3616964546018497ff6a93  linux-postmarketos-qcom-msm8953-e5486161bb0ce7d889d0a329a062b12864e61a56.tar.gz
21a1cb6be6b0b84a91bec8534dc3435ab555086a9434e3f778f7e5f3696569059c8d22f4903220de06a9aeb39824480da1d5b580ad5091ae471f46f8a7a0375a  config-postmarketos-qcom-msm8953.aarch64
"
